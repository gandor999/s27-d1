let http = require("http");

http.createServer(function(request, response) {
	// GET Request
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from database');
	}

	// POST Request
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database');
	}

	// PUT Method
	if(request.url == "/items" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data added to the database');
	}

	// Delete Method
	if(request.url == "/items" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data deleted in database');
	}
}).listen(4000);

console.log('Server is running at localhost:4000')